# Put this file in your home directory
options(warnPartialMatchDollar = TRUE)
options(repos = c(CRAN = "https://mirror.ibcp.fr/pub/CRAN/"))
options(editor = "code")

tibble <- tibble::tibble
as_tibble <- tibble::as_tibble
read_csv <- readr::read_csv
shead <- SKM::shead

if (interactive()) {
  options(width = 100)
  options(digits = 5)
  options(max.print = 200)
}

# More complete error tracing by default
options(error = function() {
  calls <- sys.calls()
  if (length(calls) >= 2L) {
    sink(stderr())
    on.exit(sink(NULL))
    cat("Backtrace:\n")
    calls <- rev(calls[-length(calls)])
    for (i in seq_along(calls)) {
      cat(i, ": ", deparse(calls[[i]], nlines = 1L), "\n", sep = "")
    }
  }
  if (!interactive()) {
    q(status = 1)
  }
})

# VSCode --------------------------------------------------

# Defined in VSCode user settings
if (
  identical(Sys.getenv("IS_VSCODE"), "TRUE") ||
  Sys.getenv("VSCODE_DEBUG_SESSION") == "1"
) {
  Sys.setenv(TERM_PROGRAM = "vscode")
  source(file.path(
    Sys.getenv(
      if (.Platform$OS.type == "windows") "USERPROFILE" else "HOME"
    ),
    ".vscode-R",
    "init.R"
  ))

  .First.sys()
}

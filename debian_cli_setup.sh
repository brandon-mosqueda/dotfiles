#!/usr/bin/bash

##### Make aliases work in this script
shopt -s expand_aliases

alias sln='ln'
alias sudocp='cp'
from_docker=false
as_sudo=false

for i in "$@" ; do
    if [[ $i == "--as-sudo" ]] ; then {
        alias apt='sudo apt'
        alias sln='sudo ln'
        alias rm='sudo rm'
        alias sudocp='sudo cp'
        as_sudo=true
        break
    } elif [[ $i == "--from-docker" ]] {
        from_docker=true
    }
    fi
done

##### Get started
apt update
apt upgrade

##### Install git
apt install git

# Install compilers
apt install build-essential
apt install cmake
apt install manpages-dev

# Init directories
mkdir ~/.passwords
touch ~/.passwords/login_aliases.sh
touch ~/.passwords/git.txt

##### Dotfiles repository
# Clone
cd ~ || exit
git clone https://gitlab.com/brandon-mosqueda/dotfiles.git

# Update source.list file
rm -f /etc/apt/sources.list
sudocp ~/dotfiles/sources.list /etc/apt

# Move r files to home directory
ln -s ~/dotfiles/.Renviron ~/
ln -s ~/dotfiles/.Rprofile ~/

# Git configurations
ln -s ~/dotfiles/.gitconfig ~/

# Nano custom configuration
rm -f /etc/nanorc
sln -s ~/dotfiles/nanorc /etc

# Update system repositories and packages
apt update
apt upgrade

# Fira code ligatures. VSCode will automatically recognize it.
sudo apt install fonts-firacode

##### Install and configure zsh
# Install zsh, a beutiful shell
apt install zsh
apt install wget

# Download oh-my-zsh
wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh

# Installs plugins
# Autosuggestions
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

# Sintax highlights
git clone https://github.com/zsh-users/zsh-syntax-highlighting ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

# Docker aliases
mkdir ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/docker-aliases
wget https://raw.githubusercontent.com/webyneter/docker-aliases/master/docker-aliases.plugin.zsh -O ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/docker-aliases/docker-aliases.plugin.zsh

# Import your settings
rm -f ~/.zshrc
ln -s ~/dotfiles/.zshrc ~/

# Make zsh your default shell
chsh -s "$(which zsh)"

##### Install nala, an apt prettier.
echo "deb [arch=amd64,arm64,armhf] http://deb.volian.org/volian/ scar main" | sudo tee /etc/apt/sources.list.d/volian-archive-scar-unstable.list
wget -qO - https://deb.volian.org/volian/scar.key | sudo tee /etc/apt/trusted.gpg.d/volian-archive-scar-unstable.gpg > /dev/null
sudo apt update && sudo apt install nala

##### Install apt packages
# Nano
apt install nano

# Docker
if [ "$from_docker" = false ]; then
    apt install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

    echo \
    "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
    buster stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

    apt update

    apt install docker-ce docker-ce-cli containerd.io
fi

# List directories as tree
apt install tree

# Python 3
apt install python3.9
apt install libpython3.9
apt install libpython3.9-dev
apt install python3-pip
apt install python3-distutils
apt install python3-apt

# Internet speed test from terminal
apt install speedtest-cli

# most, a command pager
apt install most

#  Show detail information about drives
apt install smartmontools hddtemp

# Utility for validate shell scripts
apt install shellcheck

# Terminal file browser
apt install nnn

# Terminal system monitor
apt install htop

# PDF tools to crop, merge, etc.
apt install pdftk

# Command Line Interface Search tool with extended regex support
apt install ripgrep

# Utility for read ssh passwords from files
apt install sshpass

# For extracting rar file
apt install unrar

# Pandoc and latex files
apt install pandoc pandoc-citeproc

# Latex
apt install texlive-full

# All R dependencies
# You may want to install the automatically tuned Atlas or the multi-threaded OpenBlas library in order to get higher performance for linear algebra operations
apt install libatlas3-base

# R base packages
apt install -t unstable r-base r-base-dev
apt install libcurl4-openssl-dev
apt install libcurl4-gnutls-dev
apt install libssl-dev
apt install libxml2-dev
python3.9 -m pip install radian
python3.9 -m pip install tf-nightly keras

##### Edit configuration files
if [ "$as_sudo" = true ]
then
    sudo su
fi

# Change swappiness value
printf "\n# Custom swappiness value (Brandon)\nvm.swappiness = 10\n" >> /etc/sysctl.conf

if [ "$as_sudo" = true ]
then
    exit
fi

##### Nano syntax highlight
curl https://raw.githubusercontent.com/scopatz/nanorc/master/install.sh | sh

# R scripts
rm ~/.nano/R.nanorc
ln -s ~/dotfiles/R.nanorc ~/.nano/
echo "include \"~/.nano/R.nanorc\"" >> ~/.nanorc

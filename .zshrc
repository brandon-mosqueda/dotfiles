# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh
export PYTHONPATH="$PYTHONPATH:$HOME/Documents/Python_own_modules"
export PATH="$PATH:$HOME/.local/bin:/opt/Zotero_linux-x86_64:/opt/netExtenderClient"
export EMAIL="brandon-mosqueda@member.fsf.org"
export EDITOR="nano"
export PERL_UNICODE=SDL

export PAGER="most"

ZSH_THEME="robbyrussell"

plugins=(
  docker-aliases
  git
  zsh-autosuggestions
  zsh-syntax-highlighting
)

source $ZSH/oh-my-zsh.sh
source $HOME/.passwords/login_aliases.sh

#My aliase
alias htopu='htop -u $USER'

alias poweroff='su -c "'"systemctl poweroff"'"'
alias restart='su -c "'"systemctl reboot"'"'

alias aptate='sudo apt update'
alias aptade='sudo apt upgrade'
alias aptall='sudo apt install -t unstable'
alias apturge='sudo apt purge'
alias aptove='sudo apt remove'
alias aptautove='sudo apt autoremove'

alias nala='sudo nala'

alias systatus='sudo systemctl status'
alias systop='sudo systemctl stop'
alias systart='sudo systemctl start'
alias sysrestart='sudo systemctl restart'
alias sysable='sudo systemctl enable'
alias sysdable='sudo systemctl disable'

alias tar-gz='tar -xvzf'
alias tar-tgz='tar -xvzf'
alias tar-bz2='tar -xvjf'
alias tar-tbz='tar -xvjf'
alias tar-tar='tar -xvf'
alias tar-xz='tar -xJvf'

alias tranes='trans :es -b'
alias tranen='trans -b'
alias dices='trans :es -d -v'
alias dicen='trans -d -v'

alias json-pretty='python -m json.tool'
alias to-pdf='lowriter --convert-to pdf'

alias audiodown='youtube-dl --extract-audio --audio-format mp3'
alias videodown='youtube-dl -f "bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4"'

alias weather='curl wttr.in'
alias google='ping www.google.com'

alias dotfiles='cd ~/dotfiles'
alias notes='cd ~/Desktop/notes'
alias up='cd ../'
alias last='cd -'
alias cd-1='cd ../'
alias cd-2='cd ../../'
alias cd-3='cd ../../../'
alias t2='tree -L 2'
alias t3='tree -L 3'
alias t4='tree -L 4'
alias t5='tree -L 5'
alias t6='tree -L 6'
alias t7='tree -L 7'
alias t8='tree -L 8'

alias ldir='ls -d */'
alias lss='ls -Ssh'
alias la='ls --color=tty -A'
alias lla='ls --color=tty -A -l'
alias lh='ls -d .*'

alias rmr='rm -r'
alias rmrf='rm -rf'

alias grst='git restore --staged'
alias gdit='git difftool -y'
alias git-store-credentials='git config --global credential.helper "store --file ~/.passwords/git.txt"'

alias toclip='xclip -sel clip' # Use as: command | toclip
alias r='radian'
alias py='python3.9'
alias pip='python3.9 -m pip'
alias docker='sudo docker'
alias speedtest='speedtest-cli'
alias purge_r='pgrep -f languageserver | tail +2 | xargs kill'

alias screenkey='screenkey -t 0.8 -p bottom -s small -m --vis-shift --opacity 0.5 &; disown;'
alias remapkeys='~/dotfiles/remapkeys.sh'
alias update_sys='~/dotfiles/update_system.sh'

alias windows-vm='sudo virsh net-start default && sudo virsh start windows10'

alias csv_to_latex='Rscript /home/bmosqueda/dotfiles/csv_to_latex.R'
alias rmtex='rm -Rfv *.out *.aux *.log *.synctex.gz'

fulltex () {
    pdflatex $1.tex && bibtex $1.aux && pdflatex $1.tex && pdflatex $1.tex &&\
    rm -rf $1.out $1.log $1.aux $1.synctex.gz
}

pdfex () {
    pdflatex $1.tex && rm -rf $1.out $1.log $1.aux $1.synctex.gz
}

cheat () { curl "cheat.sh/$1" }
findf () { find . -name "$1" -type f }
findd () { find . -name "$1" -type d }
rmra () { find . -name "$1" -type f | xargs rm}

rcmd () {
    for file in $(ls "$@");
    do {
        echo "*** Running $file ***";
        R CMD BATCH $file & disown;
        sleep 0.5;
    }
    done;
}

rscript () {
    Rscript $1 ${@:2} >& $1.Rout &; disown;
}

# This function receives a json file where the entry is an array of objects with
# the information to run an R script. The first param of each object is
# source_file with the path of the Script to run. The second param is "params",
# an object where each key is a variable to be created in the R environment.
# The cartesian product of the values of each key is created and the script is
# run with each combination of values.
jsonrscript () {
    file=$1
    out_file=$(basename "${file%.*}").Rout
    Rscript ~/.run.R $file >& $out_file &; disown;
}

ftoclip() { cat "$1" | toclip }
# MY ALIASES END

if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
  source /etc/profile.d/vte-2.91.sh
fi

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=5'

# Cd on quit nnn
n ()
{
    # Block nesting of nnn in subshells
    if [ -n $NNNLVL ] && [ "${NNNLVL:-0}" -ge 1 ]; then
        echo "nnn is already running"
        return
    fi

    # The default behaviour is to cd on quit (nnn checks if NNN_TMPFILE is set)
    # To cd on quit only on ^G, remove the "export" as in:
    #     NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"
    # NOTE: NNN_TMPFILE is fixed, should not be modified
    export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    # Unmask ^Q (, ^V etc.) (if required, see `stty -a`) to Quit nnn
    # stty start undef
    # stty stop undef
    # stty lwrap undef
    # stty lnext undef

    nnn "$@"

    if [ -f "$NNN_TMPFILE" ]; then
            . "$NNN_TMPFILE"
            rm -f "$NNN_TMPFILE" > /dev/null
    fi
}

# Run the command passed as argument in each line of the stdin
efor () {
    for line in $(cat -);
    do {
        eval "$1 $line"
        echo "*****"
    }
    done;
}

to_pdf () {
    sudo convert $1 ${1:r}.pdf
    sudo chown $USER ${1:r}.pdf
}

prepare_pdfs () {
    quality=$1
    if test -z $quality; then {
        quality=50
    } fi;

    base_dir=$2
    if test -z $2; then {
        base_dir="."
    } fi;

    echo "Quality: $quality"
    echo "Base dir: $base_dir"

    mkdir -p "$base_dir/light_images"
    mkdir -p "$base_dir/pdf"

    for image in $(ls $base_dir);
    do {
        echo $image
        convert -strip -quality $quality -rotate 90 "$base_dir/$image"\
            "$base_dir/light_images/$image"
    }
    done;

    lowriter --convert-to pdf --outdir "$base_dir"/pdf/ "$base_dir"/light_images/*
    pdfunite "$base_dir"/pdf/*.pdf "$base_dir"/pdf/output.pdf
}

# Nala autocompletion
autoload bashcompinit
bashcompinit
source /usr/share/bash-completion/completions/nala

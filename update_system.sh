#! /usr/bin/bash
##### UPDATE SYSTEM
echo "Updating the system"
sudo apt update
sudo apt upgrade
sudo apt autoremove

##### UPDATE GNOME EXTENSIONS
cd ~/.local/share/gnome-shell/extensions/

for extension in `ls`;
do {
    if [[ -d "$extension/.git" ]]; then
        echo "Updating \"$extension\" extension"
        cd $extension
        git pull --quiet
        cd ..
    fi
}
done;

##### update repositories
echo "Updating dotfiles repository"
cd ~/dotfiles
git pull --quiet
git push --quiet

echo "Updating notes repository"
cd ~/Desktop/notes
git pull --quiet
git push --quiet

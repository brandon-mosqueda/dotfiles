#!/usr/bin/bash
##### Dotfiles repository
# Templates for nautilus context meny
ln -s ~/dotfiles/Templates  ~/

##### Install VSCode
sudo apt-get install wget gpg
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
rm -f packages.microsoft.gpg

sudo apt install apt-transport-https
sudo apt update
sudo apt install code

##### Install albert
git clone --recursive https://github.com/albertlauncher/albert.git
mkdir albert-build
cd albert-build

sudo apt install qtcreator\
    qtdeclarative5-dev\
    libqt5x11extras5\
    libqt5x11extras5-dev\
    libqt5svg5-dev\
    libqt5charts5-dev\
    libmuparser-dev

cmake ../albert -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Debug
make
sudo make install

# Install qt5ct for change the settings and being able to use the same icon
# themes. You have to open QT5 settings and change them manually.
sudo apt install qt5ct

# Move configurations
ln -s ~/dotfiles/albert.conf ~/.config/albert/

##### Install apt packages
# For connect to remote desktop windows machines
sudo apt install remmina

# GNOME extensions
sudo apt install gnome-tweak-tool
sudo apt install gnome-shell-extensions

# Install GNOME 40
sudo apt -t unstable install gnome-shell gnome-backgrounds gnome-applets gnome-control-center mutter gjs

# Nautilus-python is a set of python bindings for the Nautilus Extension
# Framework.
sudo apt install python3-nautilus gir1.2-nautilus-3.0

# Pomodoro counter
    # Install the dependencies
    sudo apt-get install autoconf-archive gettext valac pkg-config\
        desktop-file-utils appstream-util libappstream-glib-dev libglib2.0-dev\
        gsettings-desktop-schemas-dev gobject-introspection\
        libgirepository1.0-dev libsqlite3-dev libgom-1.0-dev\
        libgstreamer1.0-dev libgtk-3-dev libcanberra-dev libpeas-dev\
        libappindicator3-dev

    # Install from source for GNOME-42 compatibility
    git clone -b gnome-42 https://github.com/gnome-pomodoro/gnome-pomodoro.git
    cd gnome-pomodoro

    meson . build
    meson compile -C build
    sudo meson install -C build --no-rebuild

# Mail client
sudo apt install evolution

# Spanish dictionary for some applications such as evolution
sudo apt install aspell-es

# Dependencies for media player and other extensions
sudo apt install meson gnome-common gettext libglib2.0-dev

# An application for system monitoring and optimizer
sudo apt install stacer

# An application for showing up in the screen all pressed keys
sudo apt install screenkey

# Application manager
sudo apt install synaptic

# Multiplayer snake game
sudo apt install gnome-nibbles

# GIMP Image editor
sudo apt install gimp

# Paint alternative
sudo apt install drawing

# Copy commands output to the clipboard
sudo apt install xclip

# Shotwell, an image viewer with basic editing utilities
sudo apt install shotwell

# Qpdfview a tabbed pdf viewer
sudo apt install qpdfview

# Piper, an application for configuring gamer mice
sudo apt install piper

# Etcher, an usb burner
echo "deb https://deb.etcher.io stable etcher" | \
     sudo tee /etc/apt/sources.list.d/balena-etcher.list
sudo apt-key adv --keyserver hkps://keyserver.ubuntu.com:443 \
     --recv-keys 379CE192D401AB61
sudo apt update
sudo apt install balena-etcher-electron

# Torrent client
sudo apt install qbittorrent

# Telegram
sudo apt install telegram-desktop

# xkill for killing applications with a shortcut
sudo apt install xkill

# Dconf editor for exporting and loading configurations for apṕlications
sudo apt install 'dconf-*'

##### Bluetooth configuration
sudo rm /etc/bluetooth/main.conf
ln -s ~/dotfiles/main.conf /etc/bluetooth/main.conf

##### Drivers
# bluetooth
sudo apt install 'bluez*'
sudo apt install firmware-atheros
sudo apt install bluetooth
sudo apt install blueman

# You need to unpair your devices and restart the bluetooth service
# Pulseaudio is a GUI for change the audio devices
sudo apt install pulseaudio pulseaudio-module-bluetooth pavucontrol

##### Gnome extensions
cd ~/.local/share/gnome-shell/extensions/

# Clipboard indicator (Clipboard history)
git clone https://github.com/Tudmotu/gnome-shell-extension-clipboard-indicator.git \
    clipboard-indicator@tudmotu.com
gnome-extensions enable clipboard-indicator@tudmotu.com

# Dash to Dock (Personalize completely your dash)
git clone https://github.com/micheleg/dash-to-dock.git
cd dash-to-dock
make
make install
cd ..

# Dynamic Panel Transparency (Top bar transparency when apps are minimized)
git clone https://github.com/rockon999/dynamic-panel-transparency.git \
    dynamic-panel-transparency@rockon999.github.io
gnome-extensions enable dynamic-panel-transparency@rockon999.github.io

# GSConnect, KDE connect full implementation for Gnome
git clone https://github.com/andyholmes/gnome-shell-extension-gsconnect.git
cd gnome-shell-extension-gsconnect/
meson _build .
ninja -C _build install-zip
cd ..

# Hide top bar (and bind a shortcut to show the top bar)
git clone https://github.com/mlutfy/hidetopbar.git hidetopbar@mathieu.bidon.ca
cd hidetopbar@mathieu.bidon.ca
make
cd ..
gnome-extensions enable hidetopbar@mathieu.bidon.ca


# Put windows (Move windows to left/right side, bottom/top, center or corner
# with shortcuts)
sudo apt install gir1.2-wnck-3.0
git clone https://github.com/negesti/gnome-shell-extensions-negesti.git \
    putWindow@clemens.lab21.org
gnome-extensions enable putWindow@clemens.lab21.org
